$(function () {
    layui.use(['element', 'form', 'table', 'laydate', 'util'], function () {
        var table = layui.table,
            element = layui.element,
            $ = layui.$,
            laydate = layui.laydate;

        table.render({
            elem: '#table',
            url: '/api/loginRecord/list',
            page: true,
            cols: [[
                {type: 'numbers'},
                {field: 'userAccount', sort: true, title: '学号'},
                {field: 'userNickName', sort: true, title: '用户名'},
                {field: 'ipAddress', sort: true, title: 'IP',maxWidth:'60'},
                {field: 'device', sort: true, title: '设备'},
                {field: 'osName', sort: true, title: '设备类型'},
                {field: 'browserType', sort: true, title: '浏览器',minWidth:'80'},
                {
                    field: 'createTime', sort: true, templet: function (d) {
                        return layui.util.toDateString(d.createTime);
                    }, title: '登录时间',minWidth:'180'
                }
            ]]
        });

        //时间范围
        laydate.render({
            elem: '#searchDate',
            type: 'date',
            range: true,
            theme: '#393D49'
        });

        //搜索按钮点击事件
        $(document).on('click', "#searchBtn", function () {
            doSearch();
        });

        //搜索
        function doSearch() {
            var searchDate = $("#searchDate").val().split(" - ");
            var userNickName = $("#searchUserNickName").val();
            table.reload('table', {where: {startDate: searchDate[0], endDate: searchDate[1], userName: userNickName}});
        }
    });
});