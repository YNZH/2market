layui.use(['element', 'layer', 'laytpl', 'laypage'], function () {
    var element = layui.element,
        $ = layui.jquery,
        layer = layui.layer,
        laypage = layui.laypage,
        laytpl = layui.laytpl;

    Q.reg('settings',function () {
        loadView('settings');
    }).reg('myGoods',function () {
        loadView('myGoods');
    }).reg('follows',function () {
        loadView('follows');
    }).reg('myMsg',function () {
        loadView('myMsg');
    });

    Q.init({
        index:'settings'
    });

    window.onload = function () {
        $.ajax({
            url: "user.json",
            async: false,
            dataType: "json",
            success: function (data) {
                console.log(data);
                renderUserInfo(data);
            },
            error: function () {
                console.log("失败gg  ")
            }
        });
    };

    function renderUserInfo(data) {
        var getTpl = user.innerHTML,
            view = document.getElementById('user_info');
        laytpl(getTpl).render(data, function (html) {
            view.innerHTML = html;
        })
    }

    function loadView(path) {
        $("#main-content").load("home/" + path +".html",function(){
            console.log("daadad")
        });
    }
});